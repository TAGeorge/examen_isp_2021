package Examen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

    public class Ex2 extends JFrame
    {
        JTextField field1;
        JButton button;

        public void display()
        {
            setLayout(null);

            button = new JButton("Press");
            button.setBounds(90, 100, 100, 30);
            field1 = new JTextField();
            field1.setBounds(90, 50, 150, 30);

            button.addActionListener(new ButtonAction());

            button.setVisible(true);

            add(field1);
            add(button);


        }
        Ex2()
        {
            setVisible(true);
            setTitle("Exercitiu2");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            display();
            setSize(300,200);


        }


        class ButtonAction implements ActionListener
        {
            public void actionPerformed(ActionEvent e)
            {
                String text1 = Ex2.this.field1.getText();

                String reverse = new StringBuffer(text1).reverse().toString();
                System.out.println(reverse);
            }
        }

        public static void main(String[] args)
        {
            new Ex2();
        }
    }


